import subprocess

from fbs_runtime.application_context.PyQt5 import ApplicationContext
import re
import git
from PyQt5 import uic, QtWidgets
import sys
from pathlib import Path
from git import GitCommandError
import os

"""Master Widget List"""
buttons = ["btnGetHomeDirectory", "btnInitGit", "btnStatus", "btnCheckout", "btnClone", "btnPull", "btnPush",
           "btnHardReset", "btnClear", "btnCommit", "btnAddAll", "btnSetAuth"]
list_widgets = ["listGitWidget"]
edittexts = ["editCWD", "editGitURL", "editCloneName", "editUserName", "editPassword", "editCommitMessage", "editCloneCWD"]
comboboxes = ["comboBranch"]
labels = ["lblRepoClean", "lblRepoURL", "lblRepoBranch", "lblInfoUserName", "lblInfoPassword"]
actions = ["actionResetAll", "actionCloneMode", "actionRepoMode"]
group_boxes = ["groupAuthentication", "groupCWD", "groupRepo", "groupPush", "groupClone"]

file = os.getcwd()
ui_file_path = '{0}/gitMaster.ui'.format(file)

COLOR_RED = 'background-color: rgb(255,0,0)'
COLOR_GREEN = 'background-color: rgb(0,255,0)'
COLOR_GREY = 'background-color: rgb(67,70,75)'


def parse(obj):
    return str(obj).encode("utf8").decode("utf8")


class LucasUI(QtWidgets.QMainWindow):
    # -> Buttons
    btnGetHomeDirectory = None
    btnInitGit = None
    btnStatus = None
    btnClone = None
    btnCheckout = None
    btnPull = None
    btnPush = None
    btnAddAll = None
    btnCommit = None
    btnSetAuth = None
    btnHardReset = None
    btnClear = None
    # -> List Widgets
    listGitWidget: QtWidgets.QListWidget = None
    # -> EditTexts
    editCWD: QtWidgets.QTextEdit = None
    editCloneCWD: QtWidgets.QTextEdit = None
    editGitURL: QtWidgets.QTextEdit = None
    editCloneName: QtWidgets.QTextEdit = None
    editUserName: QtWidgets.QTextEdit = None
    editPassword: QtWidgets.QTextEdit = None
    editCommitMessage: QtWidgets.QTextEdit = None
    # -> ComboBox
    comboBranch: QtWidgets.QComboBox = None
    # -> Labels
    lblRepoClean: QtWidgets.QLabel = None
    lblRepoURL: QtWidgets.QLabel = None
    lblRepoBranch: QtWidgets.QLabel = None
    lblInfoUserName: QtWidgets.QLabel = None
    lblInfoPassword: QtWidgets.QLabel = None
    # -> Actions
    actionResetAll: QtWidgets.QAction = None
    actionCloneMode: QtWidgets.QAction = None
    actionRepoMode: QtWidgets.QAction = None
    # -> Group Boxes
    groupAuthentication: QtWidgets.QGroupBox = None
    groupCWD: QtWidgets.QGroupBox = None
    groupRepo: QtWidgets.QGroupBox = None
    groupPush: QtWidgets.QGroupBox = None
    groupClone: QtWidgets.QGroupBox = None

    isHttp = True
    isCloneMode = False
    current_director = ""
    current_branch = ""
    git_url = ""
    git_auth_url = ""
    username = ""
    password = ""
    clone_name = ""
    branches = None
    git_master: git.Git = None
    git_repo: git.Repo = None

    def __init__(self):
        super(LucasUI, self).__init__()
        uic.loadUi(ui_file_path, self)
        self.bind_elements()
        self.set_btn_actions()
        self.btn_RepoMode()
        self.groupAuthentication.setVisible(False)
        self.show()

    def bind_elements(self):
        """Set Buttons"""
        for btn in buttons:
            setattr(self, btn, self.findChild(QtWidgets.QPushButton, btn))
        # -> Set List Widgets
        for list_item in list_widgets:
            setattr(self, list_item, self.findChild(QtWidgets.QListWidget, list_item))
        for edit in edittexts:
            setattr(self, edit, self.findChild(QtWidgets.QTextEdit, edit))
        for combo in comboboxes:
            setattr(self, combo, self.findChild(QtWidgets.QComboBox, combo))
        for lbl in labels:
            setattr(self, lbl, self.findChild(QtWidgets.QLabel, lbl))
        for act in actions:
            setattr(self, act, self.findChild(QtWidgets.QAction, act))
        for box in group_boxes:
            setattr(self, box, self.findChild(QtWidgets.QGroupBox, box))

    def set_btn_actions(self):
        # -> Buttons
        self.btnInitGit.clicked.connect(lambda: self.do_init())
        self.btnStatus.clicked.connect(lambda: self.btn_status())
        self.btnClone.clicked.connect(lambda: self.btn_clone())
        self.btnCheckout.clicked.connect(lambda: self.btn_checkout())
        self.btnPull.clicked.connect(lambda: self.btn_pull())
        self.btnGetHomeDirectory.clicked.connect(lambda: self.btn_get_home())
        self.btnHardReset.clicked.connect(lambda: self.btn_hard_reset())
        self.btnClear.clicked.connect(lambda: self.btn_btnClear())
        self.btnPush.clicked.connect(lambda: self.btn_push())
        self.btnCommit.clicked.connect(lambda: self.btn_btnCommit())
        self.btnAddAll.clicked.connect(lambda: self.btn_btnAddAll())
        self.btnSetAuth.clicked.connect(lambda: self.btn_btnSetAuth())
        # -> Action Buttons
        self.actionResetAll.triggered.connect(lambda: self.reset_all())
        self.actionCloneMode.triggered.connect(lambda: self.btn_CloneMode())
        self.actionRepoMode.triggered.connect(lambda: self.btn_RepoMode())

    def get(self, var_name):
        """  GETTER HELPER  """
        return self.__getattribute__(var_name)

    """ -> INITIAL SETUP <- """
    def do_init(self):
        # 1. Set Current Directory
        self.set_current_directory()
        # 2. Set Git Repo
        self.set_git_repo()
        # 3. Set Branches
        self.set_branches()
        # 4. Set Git Info
        self.set_git_info()
        # Show User The Repo Directory Set
        self.listGitWidget.addItem("REPO: = " + self.current_director)

    # 1. Set Current Directory
    def set_current_directory(self):
        self.current_director = self.editCWD.toPlainText()
        if self.current_director == "":
            self.editCWD.setText("Set Repo Directory!")
            return
        self.git_master = git.Git(self.current_director)

    # 2. Set Git Repo
    def set_git_repo(self):
        try:
            self.git_repo = git.Repo(self.current_director)
        except Exception:
            self.listGitWidget.addItem("NOT A REPO")
            self.clear_groupbox_repo()
            return

    # 3. Set Branches
    def set_branches(self):
        self.branches = self.git_repo.branches
        self.comboBranch.clear()
        for branch in self.branches:
            print(branch)
            self.comboBranch.addItem(str(branch))
        self.current_branch = str(self.git_repo.active_branch)
        self.comboBranch.setCurrentText(self.current_branch)

    # 4. Set Git Info
    def set_git_info(self):
        if self.is_repo_clean():
            self.lblRepoClean.setText("Clean")
            self.toggle_push_feature(False)
        else:
            self.lblRepoClean.setText("Not Clean")
            self.toggle_btnCommit(False)
            self.toggle_btnPush(False)
        self.lblRepoBranch.setText(self.current_branch)
        url = self.git_repo.git.remote("get-url", "--all", "origin")
        self.git_url = url
        self.lblRepoURL.setText(self.git_url)
        if self.git_url[:8].startswith("http"):
            self.isHttp = True
            self.parse_url()
        else:
            self.isHttp = False

    def clear_groupbox_repo(self):
        self.lblRepoClean.setText("No Repo Set")
        self.lblRepoBranch.setText("No Repo Set")
        self.lblRepoURL.setText("No Repo Set")
        self.lblInfoUserName.clear()
        self.lblInfoPassword.clear()

    def reset_all(self):
        self.git_master = None
        self.git_repo = None
        self.clear_groupbox_repo()
        self.editCWD.clear()
        self.btn_btnClear()
        self.comboBranch.clear()
        self.editUserName.clear()
        self.editPassword.clear()

    def parse_url(self):
        i, u = 0, 0
        for char in self.git_url:
            if char == ':':
                self.username = self.git_url[8:i]
                self.lblInfoUserName.setText(self.username)
                u = i
            elif char == '@':
                self.password = self.git_url[u:i]
                self.lblInfoPassword.setText(self.password)
            i += 1
        if self.username == "":
            # Needs auth
            self.groupAuthentication.setVisible(True)

    """ -> BUTTONS <- """
    def btn_CloneMode(self):
        self.isCloneMode = True
        self.groupClone.setVisible(True)
        self.groupRepo.setVisible(False)
        self.groupAuthentication.setVisible(True)
        self.groupCWD.setVisible(False)
        self.groupPush.setVisible(False)

    def btn_RepoMode(self):
        self.isCloneMode = False
        self.groupClone.setVisible(False)
        self.groupRepo.setVisible(True)
        self.groupCWD.setVisible(True)
        self.groupPush.setVisible(True)

    def btn_get_home(self):
        self.editCWD.setText(str(Path.home()))
        self.current_director = str(Path.home())

    def btn_btnSetAuth(self):
        if self.isCloneMode:
            self.btn_btnSetAuthClone()
            return
        if self.username != "":
            return
        start = self.git_url[:8]
        finish = self.git_url[8:]
        self.username = self.editUserName.toPlainText()
        self.password = self.editPassword.toPlainText()
        self.git_auth_url = "{0}{1}:{2}@{3}".format(start, self.username, self.password, finish)
        self.lblRepoURL.setText(self.git_auth_url)
        self.lblInfoUserName.setText(self.username)
        self.lblInfoPassword.setText(self.password)
        self.editUserName.clear()
        self.editPassword.clear()

    # -> STATUS
    def btn_status(self):
        self.listGitWidget.addItem("--STATUS--")
        self.listGitWidget.addItem(self.validate_and_return_repo_status())
        if self.is_repo_clean():
            # disable push
            self.toggle_btnAddAll(False)
            self.toggle_btnCommit(False)
            self.toggle_btnPush(False)
        else:
            # enable push
            self.toggle_btnAddAll(True)
            self.toggle_btnCommit(False)
            self.toggle_btnPush(False)

    # -> 1. ADD ALL
    def btn_btnAddAll(self):
        try:
            a = self.git_repo.git.add('--all')
            self.listGitWidget.addItem("Successfully added files.")
            self.listGitWidget.addItem(a)
            self.toggle_btnAddAll(False)
            self.toggle_btnCommit(True)
            self.toggle_btnPush(False)
        except Exception:
            self.listGitWidget.addItem("Failed to Add files.")

    # -> 2. COMMIT
    def btn_btnCommit(self):
        com_mess = self.editCommitMessage.toPlainText()
        if com_mess == "":
            self.listGitWidget.addItem("Please enter a commit message.")
            return
        try:
            c = self.git_repo.git.commit('-m', com_mess, author=self.username)
            self.listGitWidget.addItem("Successfully Commited")
            self.listGitWidget.addItem(c)
            self.editCommitMessage.clear()
            self.toggle_btnCommit(False)
            self.toggle_btnPush(True)
        except Exception:
            self.listGitWidget.addItem("Failed to Commit!")

    # -> 3. PUSH
    def btn_push(self):
        if self.isHttp:
            try:
                # origin = self.git_repo.remote(name='origin')
                # with origin.config_writer as cw:
                #     cw.set("url", self.git_auth_url)
                cmd = ['git', 'push', 'origin', self.current_branch]
                s = subprocess.Popen(cmd, stdout=subprocess.PIPE)
                self.listGitWidget.addItem("Successfully Pushed Code")
                self.toggle_btnPush(False)
                self.toggle_btnAddAll(True)
            except Exception:
                self.listGitWidget.addItem("Failed to Push Code")

    def btn_pull(self):
        self.listGitWidget.addItem("--PULL--")
        try:
            p = self.git_master.pull('origin', self.current_branch)
            self.listGitWidget.addItem(p)
        except Exception:
            self.listGitWidget.addItem("Pull Failed")

    # -> CLONE
    def btn_clone(self):
        self.git_url = self.editGitURL.toPlainText()
        self.clone_name = self.editCloneName.toPlainText()
        clone_dir = self.editCloneCWD.toPlainText()
        if self.git_url == "":
            self.listGitWidget.addItem("Please provide a valid git URL")
            return
        if self.clone_name == "":
            self.listGitWidget.addItem("Please provide a valid Name for your Cloned Repo")
            return
        if clone_dir == "":
            self.listGitWidget.addItem("Please Init New Git Directory")
            return
        if not os.path.exists(clone_dir):
            self.listGitWidget.addItem("Please Provide a Valid Directory")
            return
        if not self.git_url.startswith("http") or not self.git_url.endswith(".git"):
            self.listGitWidget.addItem("Please Provide a Valid Git URL")
            return
        if not self.is_valid_repo(clone_dir):
            try:
                git_master = git.Git(clone_dir)
                c = git_master.clone(self.git_url, self.clone_name)
                self.listGitWidget.addItem("Clone Successful!")
                self.listGitWidget.addItem(c)
                clone_dir = clone_dir + "/" + self.clone_name
                self.git_master = git.Git(clone_dir)
                self.btn_RepoMode()
                self.editCWD.setText(clone_dir)
                self.editUserName.clear()
                self.editPassword.clear()
                self.do_init()
                try:
                    self.listGitWidget.addItem(self.git_master.status())
                except GitCommandError:
                    self.listGitWidget.addItem("Failed to Clone Repo")
                    self.listGitWidget.addItem("Might need to Set Auth")
            except GitCommandError:
                self.listGitWidget.addItem("Clone Failed")
                self.listGitWidget.addItem("Might need to Set Auth")

    # -> CHECKOUT
    def btn_checkout(self):
        self.listGitWidget.addItem("--CHECKOUT--")
        try:
            p = self.git_master.checkout(self.comboBranch.currentText())
            self.listGitWidget.addItem(p)
        except Exception:
            self.listGitWidget.addItem("Checkout Failed")

    # -> HARD RESET
    def btn_hard_reset(self):
        self.git_repo.git.reset('--hard')
        self.do_init()

    def btn_btnClear(self):
        self.listGitWidget.clear()
        self.editGitURL.clear()
        self.editCloneName.clear()

    """ -> UTILITY METHODS <- """
    def toggle_push_feature(self, enabled):
        self.btnAddAll.setEnabled(enabled)
        self.btnCommit.setEnabled(enabled)
        self.btnPush.setEnabled(enabled)

    def toggle_btnAddAll(self, enabled):
        self.btnAddAll.setEnabled(enabled)

    def toggle_btnCommit(self, enabled):
        self.editCommitMessage.setEnabled(enabled)
        self.btnCommit.setEnabled(enabled)

    def toggle_btnPush(self, enabled):
        self.btnPush.setEnabled(enabled)

    def is_repo_clean(self):
        if self.git_repo.is_dirty(untracked_files=True):
            return False
        else:
            return True

    def validate_and_return_repo_status(self):
        try:
            response = self.git_master.status()
            return response
        except GitCommandError:
            return "NOT A REPO!"

    def btn_btnSetAuthClone(self):
        if self.username != "":
            return
        if self.editGitURL.toPlainText() == "":
            self.listGitWidget.addItem("Need Git Url!")
            return
        cur_url = self.editGitURL.toPlainText()
        start = cur_url[:8]
        finish = cur_url[8:]
        self.username = self.editUserName.toPlainText()
        self.password = self.editPassword.toPlainText()
        self.git_url = f"{start}{self.username}:{self.password}@{finish}"
        self.editGitURL.clear()
        self.editGitURL.setText(self.git_url)

    @staticmethod
    def is_valid_repo(dir):
        newG = git.Git(dir)
        try:
            response = newG.status()
            return True
        except GitCommandError:
            response = "NOT A REPO!"
            return False

    def loop_string(self, text):
        # decoded_text = text.decode('ascii')
        decoded_text = text.decode("utf8")
        formatted_text = re.sub(' +', ' ', decoded_text)
        list_of_items = []
        last_character = ''
        build_string = ""
        slash = "~"
        for char in formatted_text.replace("\n", "~~"):
            if char == slash and last_character == slash:
                list_of_items.append(build_string)
                build_string = ""
                last_character = char
            else:
                build_string += char
                last_character = char
        return list_of_items


if __name__ == '__main__':
    appctxt = ApplicationContext()       # 1. Instantiate ApplicationContext
    app = QtWidgets.QApplication(sys.argv)
    window = LucasUI()
    exit_code = appctxt.app.exec_()      # 2. Invoke appctxt.app.exec_()
    sys.exit(exit_code)